package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"gitlab.com/gosave/storage.git/impls"
	"io/ioutil"
	"net/http"
	"strings"
)

const scheme = "http://"

const (
	defaultHost = "127.0.0.1"
	defaultPort = "1234"
)

const (
	actionCheck = "check"
	actionPut   = "put"
	actionGet   = "get"
)

var (
	flNodeHost = flag.String("host", defaultHost, "node host")
	flNodePort = flag.String("port", defaultPort, "node port")
)

type client struct {
	stor impls.Storager
	exec *http.Client
	conn *conn
}

type conn struct {
	urlInput string
	urlCheck string
	urlPut   string
	urlGet   string
}

func init() {
	flag.Parse()
}

func (c *client) check() (err error) {
	r, err := c.exec.Head(c.conn.urlCheck)
	if err != nil {
		return
	}
	r.Body.Close()
	return
}

func (c *client) communicate() {
	args := func() ([]string, []interface{}) {
		inputS := make([]string, 3, 3)
		inputI := make([]interface{}, len(inputS), cap(inputS))

		for i := range inputI {
			inputI[i] = &inputS[i]
		}
		return inputS, inputI
	}
	argsS, argsI := args()

	for {
		fmt.Printf("%s > ", c.conn.urlInput)

		_, _ = fmt.Scanln(argsI...)

		switch argsS[0] {
		case "exit":
			fmt.Println("bye bye")
			return
		case "help":
			fmt.Println("put - put key value\nget - get key\nexit - exit")
		case actionPut:
			st, err := c.put(argsS)
			if err != nil {
				fmt.Printf("error: %s\n", err.Error())
				continue
			}
			fmt.Println(st)
		case actionGet:
			val, err := c.get(argsS)
			if err != nil {
				fmt.Printf("error: %s\n", err.Error())
				continue
			}
			fmt.Println(val)
		default:
			fmt.Println("enter help for reference")
		}
		argsS, argsI = args()
	}
}

func (c *client) put(ss []string) (string, error) {
	req := &impls.RequestPut{
		Key:   ss[1],
		Value: ss[2],
	}
	resp := &impls.ResponsePut{}

	b, err := json.Marshal(req)
	if err != nil {
		return "", err
	}

	r, err := c.exec.Post(c.conn.urlPut, "application/json", bytes.NewBuffer(b))
	if err != nil {
		return "", err
	}

	b, err = ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(b, resp)
	if err != nil {
		return "", err
	}

	if resp.ErrMsg != "" {
		return "", errors.New(resp.ErrMsg)
	}

	return resp.Status, nil
}

func (c *client) get(ss []string) (string, error) {
	req := &impls.RequestGet{
		Key: ss[1],
	}
	resp := &impls.ResponseGet{}

	b, err := json.Marshal(req)
	if err != nil {
		return "", err
	}

	r, err := c.exec.Post(c.conn.urlGet, "application/json", bytes.NewBuffer(b))
	if err != nil {
		return "", err
	}

	b, err = ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(b, resp)
	if err != nil {
		return "", err
	}

	if resp.ErrMsg != "" {
		return "", errors.New(resp.ErrMsg)
	}

	return resp.Value, nil
}

func main() {
	c := &client{
		exec: &http.Client{},
		conn: &conn{
			urlInput: url(*flNodeHost, ":", *flNodePort),
			urlCheck: url(scheme, *flNodeHost, ":", *flNodePort, "/", actionCheck),
			urlPut:   url(scheme, *flNodeHost, ":", *flNodePort, "/", actionPut),
			urlGet:   url(scheme, *flNodeHost, ":", *flNodePort, "/", actionGet),
		},
	}
	err := c.check()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	c.communicate()
}

func url(ss ...string) string {
	return strings.Join(ss, "")
}
